import React from 'react';
import { HashRouter, Route, Switch } from 'react-router-dom';

const ToDO = React.lazy(() => import('./views/todo/ToDO'));

const loading = (
	<div className="pt-3 text-center">
		<div className="sk-spinner sk-spinner-pulse"></div>
	</div>
);



function App() {
	return (
		<HashRouter>
			<React.Suspense fallback={loading}>
				<Switch>
					<Route exact path="/" name="todo-list" render={(props) => <ToDO {...props} />} />
					<Route exact path="/todo-list" name="todo-list" render={(props) => <ToDO {...props} />} />
				</Switch>
			</React.Suspense>
		</HashRouter>
	);
}

export default App;
