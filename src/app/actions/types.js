export const GET_ERRORS = 'GET_ERRORS';
export const POST_TODOS = 'POST_TODOS';
export const GET_TODOS = 'GET_TODOS';
export const PUT_TODOS = 'PUT_TODOS';
export const DELETE_TODOS = 'DELETE_TODOS';
