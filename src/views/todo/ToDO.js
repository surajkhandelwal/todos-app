import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';

import { toDoList,toDoCreate,toDoUpdate,toDoDelete } from '../../app/actions/todoAction';

import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

function ToDo(props) {

    const { apiRes } = props;

    const [title, setTitle] = useState('');
	const [description, setDescription] = useState('');
    const [id, setId] = useState(0);
    const [update, setUpdate] = useState(false);


    const [list, setList] = useState([]);
    let textInput = null;

    useEffect(() => {
		props.toDoList();
	}, []);

  

    useEffect(() => {
       
        setTitle('')
        setDescription('')
        if(apiRes.type === "GET_TODOS"){
            setList(apiRes.list);
        }
        else if(apiRes.type === "GET_ERRORS"){
            toast.error("Something Went Wrong!");
        }
        else{
            props.toDoList();
        }
	}, [apiRes]);


    const _addForm = (event) => {
		event.preventDefault();
        props.toDoCreate({title,description})
	};

    const _updateForm = (event) => {
		event.preventDefault();
        setUpdate(false)
        props.toDoUpdate(id,{title,description})
	};


    const _editToDO = (name,text,i) => {
		setTitle(name)
        setDescription(text)
        setId(i)
        setUpdate(true)
        textInput.focus();
	};

    const _deleteToDO = (event,id) => {
		event.preventDefault();
        props.toDoDelete(id)
	};

	return (
        
        <div>
            {/* navbar start  */}
            <div className="navbar navbar-expand flex-column flex-md-row align-items-center">
                <div className="container-fluid border-top border-bottom">
                    <Link to="/" className="navbar-brand mr-0 mr-md-2 logo text-dark ml-3">
                        TODO App
                    </Link>
                    <ul className="navbar-nav flex-row ml-auto d-flex align-items-center list-unstyled mb-0">
                        <li className="nav-item border p-1 bg-light mr-3">
                            ToDo List
                        </li>
                    </ul>
                </div>
            </div>
            {/* navbar */}

            {/* main content start */}

            <div className="container-fluid mt-2">
                <div className="container">

                    {/* todo form start */}

                    <div className="card border-0">
                        <div className="card-header p-2">
                            <h6 className="ml-3">Note Details</h6>
                        </div>

                        <div className="card-body mt-3  border-bottom">
                            <form onSubmit={update ? _updateForm : _addForm} method="post">
                                <div className="form-group">
                                    <label htmlFor="title">Title</label>
                                    <input
                                        className="form-control"
                                        id="title"
                                        type="text"
                                        placeholder="Title"
                                        value={title}
                                        required
                                        onChange={(e) => setTitle(e.target.value)}
                                        ref={(button) => { textInput = button; }}
                                    />
                                </div>
                                <div className="form-group">
                                    <label htmlFor="description">Description</label>
                                    <input
                                        className="form-control"
                                        id="description"
                                        type="text"
                                        placeholder="Enter Description"
                                        value={description}
                                        required
                                        onChange={(e) => setDescription(e.target.value)}
                                    />
                                </div>
                                <button type="submit" className="btn btn-primary">Submit</button>
                            </form>
                        </div>
                    </div>

                    {/* todo form end */}

                    {/* todo list start */}

                    <div className="card border-0 mt-3">
                        <div className="card-header p-2">
                            <h6 className="ml-3">TODO List</h6>
                        </div>
                       

                        <div className="card-body mt-3  border-bottom">

                            <div className="row justify-content-center">


                                {list && list.length ? list.map((item,i)=>{
                                    return (
                                    <div className="card col-lg-3 m-1" key={i}>
                                        <div className="card-body">
                                            <h5 className="card-title">{item.title}</h5>
                                            <p className="card-text">{item.description}</p>
                                            <div className="justify-content-around row">
                                                <button className="btn btn-info" onClick={(e)=>_editToDO(item.title,item.description,item.id)}>Edit</button>
                                                <button className="btn btn-danger" onClick={(e)=>{
                                                    if (
                                                        window.confirm(
                                                            'All your changes will be lost. Do you still want continue?'
                                                        )
                                                    ) {
                                                        _deleteToDO(e,item.id)
                                                    }
                                                }}>Delete</button>
                                            </div>
                                            
                                        </div>
                                    </div>
                                    )
                                }) : 'No Data Found!'}


                            </div>
                            
                        </div>
                    </div>

                    {/* todo list end */}




                </div>
            </div>

            {/* main content */}
            <ToastContainer />
        </div>
	);
}

ToDo.propTypes = {
	toDoList : PropTypes.func.isRequired,
    toDoCreate : PropTypes.func.isRequired,
    toDoUpdate : PropTypes.func.isRequired,
    toDoDelete : PropTypes.func.isRequired,
    apiRes: PropTypes.object.isRequired
};

const mapStateToProps = (state) => ({
    apiRes: state.apiRes
});

export default connect(mapStateToProps, { toDoList,toDoCreate,toDoUpdate,toDoDelete })(ToDo);
